#ifndef GAMETIMER_HPP
#define GAMETIMER_HPP

#include <QDebug>
#include <QObject>
#include <QTimer>

class GameTimer : public QObject {
    Q_OBJECT

  public:
    GameTimer();
    QTimer* timer;

  public slots:
    void timeTickSlot();

  signals:
    void timeTickSignal();
};

#endif // GAMETIMER_HPP
