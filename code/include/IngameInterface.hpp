#ifndef INGAMEINTERFACE_HPP
#define INGAMEINTERFACE_HPP

#include "CustomView.hpp"
#include "InterfaceTowerSlot.hpp"

#include <QPushButton>

class IngameInterface {
  public:
    IngameInterface(CustomView* view);
    ~IngameInterface();
    void showInterface();
    void hideInterface();

  private:
    QVector<InterfaceTowerSlot*> towerSlots;

    QString style = "QPushButton {"
                    "border-image:url(:/images/images/normal.png);"
                    "min-width: 64;"
                    "min-height: 64;"
                    "font: 15px Arial, sans-serif;"
                    "color: white;"
                    "}";
};

#endif // INGAMEINTERFACE_HPP
