#include <code/include/Game.hpp>
#include <code/include/Mapper.hpp>
#include <code/include/Tower.hpp>

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QFont>
#include <QGLWidget>
#include <QGraphicsTextItem>
#include <QInputDialog>
#include <QLineEdit>
#include <QMediaPlayer>
#include <QMessageBox>
#include <QObject>
#include <QPointer>
#include <QScreen>
#include <QTextStream>

#include <code/include/Bat.hpp>
#include <code/include/IngameInterface.hpp>
#include <code/include/Skeleton.hpp>
#include <code/include/TextItem.hpp>
#include <code/include/Vampire.hpp>

Game* Game::instance = nullptr;

Game::Game() {
    // qDebug() << "Constructor called.";
}

auto Game::game() -> Game& {
    if (!instance)
        instance = new Game();
    return *instance;
}

void Game::menuScreen() {
    // Add a smaller scene with a view to
    // show the starting menu witha button
    // Button deletes the main menu and starts the game
    // scene and view

    scene = new QGraphicsScene();
    scene->setSceneRect(-width / 2, -height / 2, width - 50, height - 50);

    background_image = new QGraphicsPixmapItem(
        QPixmap(":/images/images/battleback5.png").scaled(width, height));
    background_image->setPos(-width / 2 - 25, -height / 2 - 25);
    scene->addItem(background_image);

    view = new CustomView(scene);

    view->setMaximumSize(width, height);
    view->setMinimumSize(width, height);
    view->centerOn(0, 0);

    initButtonSound();

    startGameBtn = new QPushButton("Start Game", view);
    startGameBtn->setGeometry(QRect(QPoint(440, 300), QSize(150, 50)));
    startGameBtn->setStyleSheet(style);

    loadMapButton = new QPushButton("Change map", view);
    loadMapButton->setGeometry(QRect(QPoint(440, 375), QSize(150, 50)));
    loadMapButton->setStyleSheet(style);

    exitButton = new QPushButton("Exit", view);
    exitButton->setGeometry(QRect(QPoint(440, 450), QSize(150, 50)));
    exitButton->setStyleSheet(style);

    QObject::connect(startGameBtn, SIGNAL(clicked()), this,
                     SLOT(playButtonSound()));
    QObject::connect(startGameBtn, SIGNAL(released()), this,
                     SLOT(startSecondScene()));

    QObject::connect(loadMapButton, SIGNAL(clicked()), this,
                     SLOT(playButtonSound()));
    QObject::connect(loadMapButton, SIGNAL(released()), this,
                     SLOT(startThirdScene()));

    QObject::connect(exitButton, SIGNAL(clicked()), this,
                     SLOT(playButtonSound()));
    QObject::connect(exitButton, SIGNAL(released()), this, SLOT(yesOrNoQuit()));

    startGameBtn->show();
    loadMapButton->show();
    exitButton->show();

    view->show();
}

void Game::yesOrNoQuit() {
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(exitButton, "Exit test",
                                  "Are you sure you want to exit the game?",
                                  QMessageBox::Yes | QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        qDebug() << "Yes was clicked";
        QTimer::singleShot(200, this, SLOT(quitGame()));
    } else
        qDebug() << "No was clicked";
}

// Checking if the buttonSound is already playing
// If not then it should start playing
void Game::playButtonSound() {
    if (buttonSound->state() == QMediaPlayer::PlayingState) {
        buttonSound->setPosition(0);
    } else if (buttonSound->state() == QMediaPlayer::StoppedState) {
        buttonSound->play();
    }
}

void Game::startSecondScene() {

    startGameBtn->hide();
    loadMapButton->hide();
    exitButton->hide();
    // scene->removeItem(background_image);
    initScreen();
    initGraphics();
    initMap();
    initScore();
    initGold();
    initHealth();
    initIngameInterface();
    initBackgroundMusic();
    beginGame();
}

void Game::startThirdScene() {

    QString temp = QFileDialog::getOpenFileName(
        loadMapButton, tr("Open .txt file"), "/", tr("Text files (*.txt)"));

    if (temp != nullptr) {
        mapChoice = temp;
        QMessageBox::information(loadMapButton, "Success",
                                 "The map was successfully loaded",
                                 QMessageBox::Ok, 0);
    } else {
        QMessageBox::information(loadMapButton, "Failure",
                                 "The map was not successfully loaded",
                                 QMessageBox::Ok, 0);
    }
    // exitButton->hide();
}

void Game::setApp(QApplication* app) { gameApp = app; }

auto Game::getGameApp() -> QApplication* { return gameApp; }

void Game::launchGame() { menuScreen(); }

// Simple screen initialization. All relevant pointers are set.
void Game::initScreen() {
    scene = new QGraphicsScene();

    // This line makes the scene coordinate system -1600 -> 1600 on both
    // axes
    scene->setSceneRect(-sceneWidth / 2, -sceneWidth / 2, sceneWidth,
                        sceneWidth);
    scene->setItemIndexMethod(scene->NoIndex);
    // view = new QGraphicsView(scene);

    view->setScene(scene);
    view->setViewport(new QGLWidget());
    view->setRenderHint(QPainter::SmoothPixmapTransform);
    view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    view->setOptimizationFlags(QGraphicsView::DontSavePainterState);
    // view->setRenderHint(QPainter::Antialiasing);

    pauseButton = new QPushButton("Pause", view);
    pauseButton->setGeometry(QRect(QPoint(850, 100), QSize(150, 50)));
    pauseButton->setStyleSheet(style);

    restartButton = new QPushButton("Restart", view);
    restartButton->setGeometry(QRect(QPoint(850, 150), QSize(150, 50)));
    restartButton->setStyleSheet(style);
    resumeButton = new QPushButton("Resume", view);
    resumeButton->setGeometry(QRect(QPoint(850, 100), QSize(150, 50)));
    resumeButton->setStyleSheet(style);

    QObject::connect(resumeButton, SIGNAL(clicked()), this,
                     SLOT(playButtonSound()));
    QObject::connect(resumeButton, SIGNAL(released()), this, SLOT(resume()));

    QObject::connect(pauseButton, SIGNAL(clicked()), this,
                     SLOT(playButtonSound()));
    QObject::connect(pauseButton, SIGNAL(released()), this, SLOT(pause()));

    QObject::connect(restartButton, SIGNAL(clicked()), this,
                     SLOT(playButtonSound()));
    QObject::connect(restartButton, SIGNAL(released()), this, SLOT(restart()));

    pauseButton->show();
    restartButton->show();

    /* fullscreen test

    QScreen *screen = QGuiApplication::primaryScreen();
    QRect  screenGeometry = screen->geometry();
    height = screenGeometry.height();
    width = screenGeometry.width();

    view->setMaximumSize(width, height);
    view->setMinimumSize(width, height);

    view->showFullScreen();
    */
}

void Game::initMap() { currentMap = new Map(new Mapper(mapChoice)); }

void Game::initGraphics() {
    spriteLoader = new SpriteLoader("units", "towers", "misc");
}

//
void Game::beginGame() {
    gameTimer = new QTimer(this);
    gameTimer->start(16);
    view->enableMouseMovement();

    connect(gameTimer, &QTimer::timeout, this, &Game::spawnWave);

    qDebug() << "Game begins.";
}

// Spawn enemies on every 16 seconds
void Game::spawnWave() {
    elapsedSpawnTime += 16;

    if (elapsedSpawnTime % 16000 == 0 || elapsedSpawnTime == 16) {

        // Every wave buff the units

        if (elapsedSpawnTime % (16000 * 4) == 0 &&
            elapsedSpawnTime <= 16000 * 20) {
            units++;
        }

        if (elapsedSpawnTime % (16000 * 3) == 0) {
            enemyBuff += 15;
        }

        spriteLoader->buffEnemyUnits(enemyBuff, enemyBuff);

        new Bat(currentMap->unitSpawnPointer->pos());

        for (int i = 0; i < units; i++) {
            new Skeleton(currentMap->unitSpawnPointer->pos());
            new Vampire(currentMap->unitSpawnPointer->pos());
        }
    }
}

void Game::pause() {
    qDebug() << "Game paused";
    ingameInterface->hideInterface();
    pauseButton->hide();
    restartButton->hide();
    backgroundMusic->pause();
    resumeButton->show();
    isPaused = !isPaused;
    gameTimer->stop();
}

void Game::restart() {
    qDebug() << "Game restarted";
    gameTimer->stop();
    backgroundMusic->stop();
    startSecondScene();
}

void Game::resume() {
    qDebug() << "Game resumed";
    resumeButton->hide();
    pauseButton->show();
    ingameInterface->showInterface();
    restartButton->show();
    backgroundMusic->play();
    isPaused = !isPaused;
    gameTimer->start(16);
}

void Game::onNexusDead() {
    isFinished = true;
    gameTimer->stop();
    backgroundMusic->stop();
    ingameInterface->hideInterface();
    restartButton->hide();
    pauseButton->hide();
    int currentScore = score->getScore();
    scene = new QGraphicsScene();
    scene->setSceneRect(-width / 2, -height / 2, width - 50, height - 50);

    background_image = new QGraphicsPixmapItem(
        QPixmap(":/images/images/graveyard.jpeg").scaled(width, height));
    background_image->setPos(-width / 2 - 25, -height / 2 - 25);
    scene->addItem(background_image);

    auto* io = new TextItem;
    io->setPos(-150, -250);
    io->setPlainText("Your score: " + QString::number(currentScore));

    QFont f;
    f.setPointSize(28);
    f.setItalic(true);
    f.setWeight(QFont::Bold);
    f.setStyleHint(QFont::Helvetica);

    io->setFont(f);

    io->setDefaultTextColor(1);

    scene->addItem(io);

    view->setScene(scene);

    view->setMaximumSize(width, height);
    view->setMinimumSize(width, height);
    view->centerOn(0, 0);

    backToMainMenuButton = new QPushButton("Back to main menu", view);
    backToMainMenuButton->setGeometry(QRect(QPoint(400, 250), QSize(200, 50)));
    backToMainMenuButton->setStyleSheet(style);

    tryAgainButton = new QPushButton("Try again", view);
    tryAgainButton->setGeometry(QRect(QPoint(400, 325), QSize(200, 50)));
    tryAgainButton->setStyleSheet(style);

    QObject::connect(backToMainMenuButton, SIGNAL(clicked()), this,
                     SLOT(playButtonSound()));
    QObject::connect(backToMainMenuButton, SIGNAL(released()), this,
                     SLOT(backToMainMenu()));

    QObject::connect(tryAgainButton, SIGNAL(clicked()), this,
                     SLOT(playButtonSound()));
    QObject::connect(tryAgainButton, SIGNAL(released()), this,
                     SLOT(backToSecondScene()));

    backToMainMenuButton->show();
    tryAgainButton->show();

    view->show();
}

void Game::backToMainMenu() {
    isFinished = false;
    view->hide();
    menuScreen();
}

void Game::backToSecondScene() {
    isFinished = false;
    startSecondScene();
}

void Game::quitGame() { gameApp->quit(); }

void Game::saveScore() {
    if (game().isFinished == true) {
        bool ok;
        QString text = QInputDialog::getText(
            view, tr("Enter your name"), tr("Player name:"), QLineEdit::Normal,
            QDir::home().dirName(), &ok);
        if (!ok || text.isEmpty()) {
            QMessageBox::information(view, "Failure",
                                     "The score was not successfully saved",
                                     QMessageBox::Ok, 0);
            return;
        }

        playerName = text;

        qDebug() << "Saving score";
        QFile file("../12-nexus-defense/highscores.txt");
        if (!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(view, "Failure",
                                     "The score was not successfully saved",
                                     QMessageBox::Ok, 0);
            return;
        }
        QTextStream in(&file);
        QMultiMap<int, QString> scores;

        while (!in.atEnd()) {
            int score;
            QString line = in.readLine();
            QStringList list = line.split(" ");
            // qDebug() << list[0] << list[1];

            bool ok;
            score = list[1].toInt(&ok);
            if (!ok) {
                QMessageBox::information(view, "Failure",
                                         "The score was not successfully saved",
                                         QMessageBox::Ok, 0);
                return;
            }
            scores.insert(score, list[0]);
        }

        file.close();
        scores.insert(score->getScore(), playerName);

        //  for(auto iter = scores.begin(); iter!= scores.end(); iter++)
        //  {
        //      qDebug() << iter.value() << " " << iter.key();
        //  }

        QVector<QPair<int, QString>> pairs;
        QPair<int, QString> pair;
        for (auto iter = scores.begin(); iter != scores.end(); iter++) {
            pair.first = iter.key();
            pair.second = iter.value();
            pairs.push_back(pair);
        }

        if (!file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(view, "Failure",
                                     "The score was not successfully saved",
                                     QMessageBox::Ok, 0);
            return;
        }

        QTextStream out(&file);

        for (auto iter = pairs.rbegin(); iter != pairs.rend(); iter++) {
            out << iter->second << " " << iter->first << endl;
        }

        file.close();
        QMessageBox::information(view, "Success",
                                 "The score was successfully saved",
                                 QMessageBox::Ok, 0);
    }
}

void Game::cleanup() {

    delete Game::game().spriteLoader;
    delete instance;

    qDebug() << "Game ends.";
}

void Game::initScore() {
    score = new Score();
    QPointF pos = view->mapToScene(0, 0);
    Game::game().score->setPos(pos.x(), pos.y() + 40);
    scene->addItem(score);
}

void Game::initGold() {
    gold = new Gold();
    QPointF pos = view->mapToScene(0, 0);
    gold->setPos(pos.x(), pos.y());
    scene->addItem(gold);
}

void Game::initHealth() {
    health = new Health();

    QPointF pos = view->mapToScene(0, 0);
    health->setPos(pos.x(), pos.y() + 20);
    scene->addItem(health);

    QObject::connect(health, SIGNAL(dead()), this, SLOT(onNexusDead()));
}

void Game::initIngameInterface() {
    ingameInterface = new IngameInterface(view);
    ingameInterface->showInterface();
}

auto Game::isTowerTile(QPointF posXY) -> bool {
    // Return if tower is buldable on tile

    if (currentMap
                ->getTilePointer(static_cast<int>(posXY.x()),
                                 static_cast<int>(posXY.y()))
                ->type == "T" ||
        currentMap
                ->getTilePointer(static_cast<int>(posXY.x()),
                                 static_cast<int>(posXY.y()))
                ->type == "TE") {
        return true;
    }

    return false;
}

void Game::initButtonSound() {
    buttonSound = new QMediaPlayer();
    buttonSound->setMedia(QUrl("qrc:/sounds/StoneButton.mp3"));
}

void Game::initBackgroundMusic() {
    auto* playlist = new QMediaPlaylist();
    playlist->addMedia(QUrl("qrc:/sounds/bgsound.mp3"));
    playlist->setPlaybackMode(QMediaPlaylist::Loop);

    backgroundMusic = new QMediaPlayer();
    backgroundMusic->setPlaylist(playlist);
    backgroundMusic->setVolume(20);
    backgroundMusic->play();
}

// TOWER SLOTS

void Game::setTower1() { towerSelected = 0; }

void Game::setTower2() { towerSelected = 1; }
void Game::setTower3() { towerSelected = 2; }
void Game::setTower4() { towerSelected = 3; }
