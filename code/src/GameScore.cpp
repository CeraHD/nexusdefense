#include <QFont>
#include <code/include/GameScore.hpp>

Score::Score(QGraphicsItem* parent) : QGraphicsTextItem(parent) {
    score = 0;

    setPlainText(QString("Score: ") + QString::number(score)); // Score: 0
    setDefaultTextColor(Qt::blue);
    setFont(QFont("times", 16));
}

void Score::increase() {
    score++;
    setPlainText(QString("Score: ") + QString::number(score)); // Score: 1
}

auto Score::getScore() -> int { return score; }
