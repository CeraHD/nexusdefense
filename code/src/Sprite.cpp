#include <code/include/Sprite.hpp>
#include <utility>

Sprite::Sprite(QString name, QString spritesheet, QString initialState,
               const int offsetX, const int offsetY, const int movementDelay,
               const int attackDamage, const int maxHealth)
    : name(std::move(name)), spritesheet(std::move(spritesheet)),
      initialState(std::move(initialState)), offsetX(offsetX), offsetY(offsetY),
      movementDelay(movementDelay), attackDamage(attackDamage),
      maxHealth(maxHealth) {}

auto Sprite::getName() const -> QString { return name; }

auto Sprite::getSpritesheet() const -> QString { return spritesheet; }

auto Sprite::getInitialState() const -> QString { return initialState; }

auto Sprite::getOffsetX() const -> int { return offsetX; }

auto Sprite::getOffsetY() const -> int { return offsetY; }

auto Sprite::getMovementDelay() const -> int { return movementDelay; }

auto Sprite::getAttackDamage() const -> int { return attackDamage; }

void Sprite::increaseAttackDamage(const int damagePercent) {
    attackDamage += static_cast<int>(attackDamage * (damagePercent / 100.0));
}

auto Sprite::getMaxHealth() const -> int { return maxHealth; }

void Sprite::increaseMaxHealth(const int healthPercent) {
    maxHealth += static_cast<int>(maxHealth * (healthPercent / 100.0));
}

auto Sprite::getStatesMap() -> QMap<QString, QVector<Sprite::frame>>& {
    return animationStates;
}
