#include <QFont>
#include <code/include/Health.hpp>

Health::Health(QGraphicsItem* parent) : QGraphicsTextItem(parent) {
    health = 100;

    // Draw the text
    setPlainText(QString("Health: ") + QString::number(health));
    setDefaultTextColor(Qt::green);
    setFont(QFont("times", 16));
}

void Health::decrease(int damageAmount) {
    health -= damageAmount;
    setPlainText(QString("Health: ") + QString::number(health));
    if (health == 0)
        emit dead();
}

auto Health::getHealth() -> int { return health; }
