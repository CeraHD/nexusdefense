#!/usr/bin/python3

import sys
import subprocess


def main():
    status = subprocess.getstatusoutput(f'git status')

    status_lines = status[1].strip().split('\n')

    for i in range(len(status_lines)):
        status_lines[i] = '# \t' + status_lines[i] + '\n'

    lines_to_write = []

    with open('./git_hooks/custom_commit_msg.txt', 'r') as read_f:
        lines = read_f.readlines()

        for line in lines:
            if line.strip() == '# ------------------------ >8 ------------------------':
                lines_to_write.append('# GIT STATUS:\n')
                for s_line in status_lines:
                    lines_to_write.append(s_line)

            lines_to_write.append(line)

    with open(sys.argv[1], 'w') as write_f:
        write_f.writelines(lines_to_write)

    sys.exit(0)


if __name__ == '__main__':
    main()
