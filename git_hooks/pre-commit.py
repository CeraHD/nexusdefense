#!/usr/bin/python3

import sys
import subprocess


def main():
    print('Checking project status...')
    make_exit = subprocess.getstatusoutput('make')
    qmake_exit = subprocess.getstatusoutput('qmake')

    if make_exit[0] == 0 and qmake_exit[0] == 0:
        print('All clear for commiting!')
        sys.exit(0)

    print('Found errors:')
    print('make:\n', make_exit, '\n', 'qmake:\n', qmake_exit)

    sys.exit(1)


if __name__ == '__main__':
    main()
