#!/usr/bin/python3

import sys
import re

rules = \
    "COMMIT MSG RULES:\n" \
    + "\tSeparate subject from body with a blank line.\n" \
    + "\tDo not end the subject line with a period.\n" \
    + "\tCapitalize the subject line and each paragraph.\n" \
    + "\tUse the imperative mood in the subject line.\n" \
    + "\tWrap lines at 72 characters.\n" \
    + "\tUse the body to explain what and why you have done something." \
    + "In most cases, you can leave out details about how a" \
    + "change has been made."


def main():
    with open(sys.argv[1], 'r') as f:
        lines = f.readlines()

        for idx, line in enumerate(lines):
            if line.strip() == '# ------------------------ >8 ------------------------':
                break

            if line.startswith('#'):
                continue

            if not valid_line(idx, line):
                show_rules()
                sys.exit(1)

            print(line)

    sys.exit(0)


def valid_line(idx, line):
    if idx == 0:
        return re.match("^[A-Z].{,48}[0-9A-z \t]$", line)
    elif idx == 1:
        return len(line.strip()) == 0
    else:
        return len(line.strip()) <= 72


def show_rules():
    print('Your commit message did no follow the rules.\n')
    print(rules)


if __name__ == '__main__':
    main()
